# KiCad Nightly Flatpak Manifest

**Caution:** *This is work in progress. You can use the manifest to build, and
run a local kicad-nightly-flatpak.*


## Introduction

Technically, this is a flatpak manifest to build the tip of the KiCad `master`
branch. It will become a nightly flatpak when the nightly automated builds are
set up, and the build results will been uploaded to a kicad-nightly flatpak repo
for interested people to consume.


## Build locally

### Prerequisites

You need `flatpak` and `flatpak-builder` (available in most distros by now), and
you need to install both the `org.freedesktop.Platform` runtime, and the
`org.freedesktop.Sdk` SDK:

```console
$ flatpak install --user flathub org.freedesktop.Platform//20.08 org.freedesktop.Sdk//20.08
```

### Clone, build, and install kicad-nightly-flatpak locally

```console
$ git clone https://gitlab.com/kicad/packaging/kicad-flatpak/kicad-nightly-flatpaks/kicad-nightly-flatpak
$ cd kicad-nightly-flatpak
$ mkdir builddir
$ flatpak-builder --user --install ./builddir org.kicad.KiCad.Nightly.yml
```

### Subsequent rebuilds

If you keep the `.flatpak-builder` directory after a successful build,
subsequent builds will use previously built modules from this cache, unless you
changed their build definitions in the manifest. If you did not change the
manifest, only KiCad itself will be rebuilt, if there are newer commits
available in the `master` branch. Rebuild as follows:

```console
$ rm -rf builddir/*
$ flatpak-builder --user --install ./builddir org.kicad.KiCad.Nightly.yml
```


## Run

You can run a locally built kicad-nightly-flatpak via

```console
$ flatpak run --user org.kicad.KiCad.Nightly
```
